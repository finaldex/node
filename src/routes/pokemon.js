const { Router } = require('express');
const getPokemonEspecies = require('../service/pokeapiservice');

const pokeRoutes = Router();

pokeRoutes.get("/", (req, response) =>{
    getPokemonEspecies((body) =>{
        response.send(body)
    }) 
})

module.exports =  pokeRoutes
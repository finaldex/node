const express = require('express')
const pokeRoutes = require('./routes/pokemon')

const app = express()
app.use(express.json())

app.use("/pokemon", pokeRoutes)

app.listen(3000)